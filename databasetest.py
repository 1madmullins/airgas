import sys
import sqlite3
try:
	database = sys.argv[1]
	command = sys.argv[2]
except SyntaxError:
	database = "HELP"
	print("")
except IndexError:
	database = "HELP"
	print("")
if database != "HELP":
	conn = sqlite3.connect(database)
	c = conn.cursor()
	# Create table
	c.execute('''CREATE TABLE Categories
             (Category text, URL text)''')
	# Insert a row of data
	c.execute(command)
	# Save (commit) the changes
	conn.commit()

	# We can also close the connection if we are done with it.
	# Just be sure any changes have been committed or they will be lost.
	conn.close()
print('==============================================')
print('               DB Query Tester')
print('==============================================')

if database == "HELP":
	print('First Argument is Database')
	print('Second argument is Statement (Double Quote it)')
	print('==============================================')
if database != "HELP":
	print('Query ', command, ' Executed, check Database')