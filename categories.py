import sys
from lxml import html
import requests
site = "http://www.airgas.com/category/viewAllCategories"
numbertocrawl = 257 #Number of categories to crawl. Tweak this for efficiency. 257 Does it as of 6/25/18
print('==============================================')
print('             Get Airgas Categories')
print('==============================================')
page = requests.get(site)
tree = html.fromstring(page.content)
x = 0
while (x > -1):
	if (x < numbertocrawl + 2): #Set number of categories plus 2 to crawl here.
		x += 1
		try:
			count = str(x) #Need to set this as an index in the DB for future updates
			test = "((//*/li[contains(@class,'sub-category')])[" + count + "]/a)/text()" #The actual XPATH, add more to a new line to get URL 
			query = tree.xpath(test)
			if str(query) != "[]":
				query = str(query)
				try:
					print(query[2:-2])
					results = query
				except IndexError:
					print('something broke here...fix it')
					#Will need to build an exit condition in the exception
		except IndexError:
			print('Done or Not Found')
			sys.exit(0)
	else:
		sys.exit(0)
