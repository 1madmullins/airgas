import sqlite3
# Set the database file
database = "airgas.db"
# Set up the connection
conn = sqlite3.connect(database)
# Create the object
c = conn.cursor()
# Create table
c.execute("CREATE TABLE Categories (category text, url text, CONSTRAINT no_duplication UNIQUE (category , url))")
c.execute("CREATE TABLE Items (itemname text, itemurl text, CONSTRAINT no_duplication UNIQUE (itemname , itemurl))")
# Save (commit) the changes
conn.commit()
# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
