import html
import os
import sys
import sqlite3
from lxml import html as XMLhtml
import requests
def catinsert( category, url ):
        try:
                #category = sys.argv[1]
                #url = sys.argv[2]
                database = "airgas.db"
                category = html.escape(category)
                url = html.escape(url)
                params = (category, url)
        except SyntaxError:
                print("")
        except IndexError:
                print("")
        conn = sqlite3.connect(database)
        c = conn.cursor()
        # Create table
        #c.execute("CREATE TABLE Categories (category text, url text)")
        # Insert a row of data
        c.execute("INSERT or REPLACE into Categories VALUES(?,?)", params)
        # Save (commit) the changes
        conn.commit()
        # We can also close the connection if we are done with it.
        # Just be sure any changes have been committed or they will be lost.
        conn.close()

site = "http://www.airgas.com/category/viewAllCategories"
sitename = "Airgas"
count = 0
print('==============================================')
print('       Updating ' + sitename + ' Categories')
print('          Please be patient...')
print('==============================================')
page = requests.get(site)
tree = XMLhtml.fromstring(page.content)
x = 0
exitcount = 0
while True:
	x += 1
	try:
		count = str(x)
		name = "((//*/li[contains(@class,'sub-category')])[" + count + "]/a)/text()"
		urlxpath = "((//*/li[contains(@class,'sub-category')])[" + count + "]/a/@href)"
		query = tree.xpath(name)
		urlquery = tree.xpath(urlxpath)
		if str(query) != "[]":
			query = str(query)
			urlquery = str(urlquery)
			try:
				query = query[2:-2]
				#print(query[2:-2])
				count = int(count)
				count = count + 1
				urlquery = urlquery[2:-2]
				#print(urlquery[2:-2])
				#os.system("python ./Create_Airgas_Database.py "+ query + " " + urlquery)
				catinsert(query, urlquery)
			except UnicodeEncodeError:
				print('*******there was a unicode error here, add handling later******')
#			print(urlquery[2:-2])
		else:
			exitcount += 1
			if exitcount == 15:
				sys.exit(0)
	except IndexError:
		print('Done or Not Found')
		sys.exit(0)
