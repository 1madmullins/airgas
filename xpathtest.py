import sys
from lxml import html
import requests
try:
	site = sys.argv[1]
	xpath = sys.argv[2]
except SyntaxError:
	site = "HELP"
	print("")
except IndexError:
	site = "HELP"
	print("")
	
print('==============================================')
print('             Xpath Query Tester')
print('==============================================')
if site == "HELP":
	print('First Argument is Site')
	print('Second argument is Xpath')
if site != "HELP":
	page = requests.get(site)
	tree = html.fromstring(page.content)
	query = tree.xpath(xpath)
	try:
		results = query[0]
	except IndexError:
		print('Done or Not Found')
		sys.exit(0)
if site == "HELP":
	print('==============================================')
if site != "HELP":
	print('Xpath Results: ', results)