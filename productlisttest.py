import sys, requests, unicodedata, shelve
from lxml import html
from django.utils.encoding import smart_text, smart_bytes
site = sys.argv[1]
sitename = "Airgas"
count = 0
print('==============================================')
print('        ' + sitename + ' Product List')
print('==============================================')
page = requests.get(site)
tree = html.fromstring(page.content)
x = 0
exitcount = 0
while True:
	x += 1
	try:
		count = str(x)
		itemname = "(//*/div[contains(@class,'description-group')]/a/@title)[" + count + "]"
		urlxpath = "(//*/div[contains(@class,'description-group')]/a/@href)[" + count + "]"
		query = tree.xpath(itemname)
		urlquery = tree.xpath(urlxpath)
		if str(query) != "[]":
			query = str(query)
			urlquery = str(urlquery)
			try:
				print(query[2:-2] + " " + urlquery[2:-2])
				#print(query[2:-2])
			except UnicodeEncodeError:
				#print('*******there was a unicode error here, add handling later******')
				utf8string = query.encode("utf8")
				utf8string = smart_bytes(utf8string)
				utf8string = str(utf8string)
				utf8string = utf8string.replace("\\xc2\\xae", "")
				utf8string = utf8string.replace("\\'","'")
				utf8string = utf8string[4:-3]
				url = urlquery.encode("utf8")
				url = smart_bytes(url)
				url = str(url)
				url = url.replace("\\xc2\\xae", "")
				url = url.replace("\\'","'")
				url = url[4:-3]
				print(utf8string + " http://www.airgas.com" + url)
			#print(query[2:-2] + " " + urlquery[2:-2])
		else:
			exitcount += 1
			if exitcount == 15:
				sys.exit(0)
	except IndexError:
		print('Done or Not Found')
		sys.exit(0)
